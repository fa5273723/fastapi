from datetime import datetime

from fastapi_users import schemas


class UserRead(schemas.BaseUser[int]):
    id: int
    email: str



class UserCreate(schemas.BaseUserCreate):
    id: int
    email: str
    salary: float
    date: str







