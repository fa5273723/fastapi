
from typing import AsyncGenerator


from fastapi import Depends
from fastapi_users.db import SQLAlchemyUserDatabase, SQLAlchemyBaseUserTable
from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.ext.declarative import DeclarativeMeta, declarative_base
from sqlalchemy.orm import sessionmaker, mapped_column

DATABASE_URL = 'sqlite+aiosqlite:///./user.db'

Base: DeclarativeMeta = declarative_base() #похоже на metadata


class User(SQLAlchemyBaseUserTable[int], Base): #модель таблицы

    id = mapped_column(Integer, primary_key=True)
    salary = mapped_column(Float, nullable=False)
    date = mapped_column(String, nullable=False)


engine = create_async_engine(DATABASE_URL)

async_session_maker = sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)





async def get_async_session() -> AsyncGenerator[AsyncSession, None]:#получение асинхронной сессии
    async with async_session_maker() as session:
        yield session


async def get_user_db(session: AsyncSession = Depends(get_async_session)):#получение пользователя
    yield SQLAlchemyUserDatabase(session, User)


